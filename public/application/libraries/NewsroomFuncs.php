<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class NewsroomFuncs
{

	public function __construct()
	{ }

	public function get_pack_status()
	{

		return array(
			'0' => 'Available',
			'1' => 'Unavailable',
			'2' => 'MWL',
			'3' => 'Hartford',
			'4' => 'New London',
			'5' => 'Out of Service'
		);
	}

	public function get_vehicle_status()
	{

		return  array(
			'0' => 'Available',
			'1' => 'Unavailable',
			'2' => 'Out of Service',
			'3' => 'Take Home Vehicle'
		);
	}
}
