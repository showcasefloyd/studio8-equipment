	<div class="row">
		<br>
		<div class="col-sm-offset-1 col-md-10 well">

			<legend>Checkout / Reserve Equipment</legend>
			<?php
			$attributes = array("class" => "form-horizontal", "id" => "equipForm", "name" => "equipForm");
			echo form_open("equipment/checkout", $attributes);
			?>

			<fieldset>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="department" class="control-label">Reporter / Photographer</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "mmj"';
							echo form_dropdown('mmj_id', $mmjs, set_value('mmj_id'), $attributes);
							?>
							<span class="text-danger"><?php echo form_error('mmj_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox ">
						<div class="col-sm-offset-1 col-md-2">
							<label for="reservedate" class="control-label">Dates</label>
						</div>

						<div class="col-md-4 col-sm-4">
							<div class="input-group ">
								<input id="reservedate" name="reservedate" placeholder="Check out" type="text" class="form-control" value="<?php echo set_value('reservedate'); ?>" readonly>
								<span class="input-group-btn">
									<button class="btn btn-default inputdatetbn" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
								</span>
							</div>

						</div>

						<div class="col-md-4 col-sm-4">
							<div class="input-group">
								<input id="reservecidate" name="reservecidate" placeholder="Check in" type="text" class="form-control" value="<?php echo set_value('reservecidate'); ?>" readonly>
								<span class="input-group-btn">
									<button class="btn btn-default inputcidatetbn" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
								</span>

							</div>

						</div>
						<div class="col-sm-offset-3 col-sm-8">
							<span class="text-danger">
								<?php echo form_error('reservedate'); ?>
							</span>
							<?php echo $this->session->flashdata('errmsg'); ?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="equip_list_id" class="control-label">Equipment</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "equipment"';
							echo form_dropdown('equip_list_id', $equipment, set_value('equip_list_id'), $attributes);
							?>
							<span class="text-danger"><?php echo form_error('equip_list_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-10">
							<p> Use this to reserve a News8 vehicle</p>
						</div>
						<div class="col-sm-offset-1 col-md-2">
							<label for="Vehicle" class="control-label">News 8 Vehicle</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "vehicle"';
							echo form_dropdown('vehicle_id', $vehicles, set_value('vehicle_id'), $attributes);
							?>
							<span class="text-danger"><?php echo form_error('vehicle_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="notes" class="control-label">Notes</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<textarea id="notes" name="notes" class="form-control" rows="3"></textarea>
							<span class="text-danger"><?php echo form_error('notes'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
						<input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Reserve"> <a href="<?php echo base_url(); ?>index.php/equipment" id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</fieldset><?php echo form_close(); ?><?php echo $this->session->flashdata('msg'); ?>
		</div>