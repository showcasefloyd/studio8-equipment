	<div class="row">
		<br>
		<div class="col-sm-offset-1 col-md-10 well">

			<legend>Add New News8 Vehicle</legend>
			<?php
			$attributes = array("class" => "form-horizontal", "id" => "vehicleForm", "name" => "vehicleForm");
			echo form_open("vehicles/addVehicle", $attributes);
			?>

			<fieldset>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="vehicle" class="control-label ">Vehicle Name</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<input id="vehicle" name="vehicle" placeholder="Vehicle" type="text" class="form-control" value="<?php echo set_value('vehicle'); ?>">
							<span class="text-danger"><?php echo form_error('vehicle'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="vehicle" class="control-label ">Out of Service</label>
						</div>
						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "available"';
							echo form_dropdown('available', $vehicle_status, '0', $attributes);
							?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="description" class="control-label">Vehicle Description</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<textarea id="description" name="description" class="form-control" rows="3"></textarea>
							<span class="text-danger"><?php echo form_error('description'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="licplate" class="control-label ">License Plate</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<input id="licplate" name="licplate" placeholder="License Plate Number" type="text" class="form-control" value="<?php echo set_value('licplate'); ?>">
							<span class="text-danger"><?php echo form_error('licplate'); ?></span>
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
						<input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Add"> <a href="<?php echo base_url(); ?>index.php/admin" id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</fieldset><?php echo form_close(); ?><?php echo $this->session->flashdata('msg'); ?>
		</div>