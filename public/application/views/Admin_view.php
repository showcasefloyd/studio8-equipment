	<div class="row">
		<div class="col-md-12 ">
			<legend>Manage Database Assets <a href="<?php echo base_url(); ?>index.php/verify/logout" id="logout-btn" class="btn btn-warning btn-sm" rel="popover" data-content="">Logout</a></legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="admin-tool-bucket admin-panel">
				<legend>Manage Equipment</legend>

				<?php foreach ($equiplist as $eq) : ?>
					<ul>
						<li><?php echo $eq->pack; ?></li>
						<li <?php echo ($eq->available == 0) ? 'class="available"' : 'class="unavailable"'; ?>>
							<?php echo $packs_status[$eq->available]; ?>
						</li>

						<li><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <a href="<?php echo base_url(); ?>index.php/equipment/updatePack/<?php echo $eq->pkey; ?>">Update</a> | <a href="#" data-href="<?php echo base_url(); ?>index.php/equipment/removePack/<?php echo $eq->pkey; ?>" class="confirm" data-toggle="modal" data-target="#confirm-delete">Remove</a></li>
					</ul>
				<?php endforeach; ?>

				<a href="<?php echo base_url(); ?>index.php/equipment/addPack"><button type="button" class="btn btn-primary btn-sm">Add New Equipment Pack</button></a>
			</div>
		</div>
		<div class="col-md-6">
			<div class="admin-tool-bucket admin-panel">
				<legend>Manage Vehicles</legend>

				<?php foreach ($vehicles as $v) : ?>
					<ul>
						<li><?php echo $v->vehicle; ?> </li>
						<li <?php echo ($v->available == 0) ? 'class="available"' : 'class="unavailable"'; ?>>
							<?php echo $vehicle_status[$v->available]; ?>
						</li>
						<li><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> <a href="<?php echo base_url(); ?>index.php/vehicles/updateVehicle/<?php echo $v->pkey_vehicle_id; ?>">Update</a> | <a href="#" data-href="<?php echo base_url(); ?>index.php/vehicles/removeVehicle/<?php echo $v->pkey_vehicle_id; ?>" class="confirm" data-toggle="modal" data-target="#confirm-delete">Remove</a></li>
					</ul>
				<?php endforeach; ?>

				<a href="<?php echo base_url(); ?>index.php/vehicles/addVehicle"><button type="button" class="btn btn-primary btn-sm">Add New Vehicle</button></a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="admin-tool-bucket admin-panel">
				<legend>Manage Reporter / Photographer </legend>

				<?php foreach ($mmjs as $mj) {
					echo "<ul><li>" . $mj->first . " " . $mj->last . "</li> ";
					echo "<li>" . $mj->department_name . "</li>";
					echo "<li><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> <a href=" . base_url() . "index.php/mmjs/update/" . $mj->pkey . ">Update</a> | <a href='#' data-href='" . base_url() . "index.php/mmjs/remove/" . $mj->pkey . "' class='confirm' data-toggle='modal' data-target='#confirm-delete' >Remove</a></li></ul>";
				} ?>

				<a href="<?php echo base_url(); ?>index.php/mmjs/add"><button type="button" class="btn btn-primary btn-sm">Add New MMJ</button></a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?php echo $this->session->flashdata('msg'); ?>
		</div>
	</div>