	<div class="row">
		<br>
		<div class="col-sm-offset-1 col-md-10 well">

			<legend>Add New Equipment Pack</legend>
			<?php
			$attributes = array("class" => "form-horizontal", "id" => "packForm", "name" => "packForm");
			echo form_open("equipment/addPack", $attributes);
			?>

			<fieldset>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="packname" class="control-label ">Pack Name</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<input id="packname" name="packname" placeholder="Pack name" type="text" class="form-control" value="<?php echo set_value('packname'); ?>">
							<span class="text-danger"><?php echo form_error('packname'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="vehicle" class="control-label ">Out of Service</label>
						</div>
						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "available"';
							echo form_dropdown('available', $packs_status, '0', $attributes);
							?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="packdesc" class="control-label">Description of the Pack</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<textarea id="packdesc" name="packdesc" class="form-control" rows="3"></textarea>
							<span class="text-danger"><?php echo form_error('packdesc'); ?></span>
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
						<input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Add"> <a href="<?php echo base_url(); ?>index.php/admin" id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</fieldset><?php echo form_close(); ?><?php echo $this->session->flashdata('msg'); ?>
		</div>