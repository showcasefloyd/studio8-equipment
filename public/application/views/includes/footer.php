	<div class="row">
		<div class="col-md-12">
			<div class="footer">
				<p>&copy; WTNH <?php echo date("Y"); ?> / Version 1.1</p>
				<p>Powered by <?php echo 'CodeIgniter Version <strong>' . CI_VERSION . '</strong>'; ?> | Bootstrap <strong>v3.3.6</strong></p>
			</div>
		</div>
	</div>

	</div>

	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<h4>Delete Record</h4>
					<p>Are you sure?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<a class="btn btn-danger btn-ok">Delete</a>
				</div>
			</div>
		</div>
	</div>

	<script src="<?php echo base_url(); ?>/scripts/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/scripts/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>/scripts/bootstrap-datetimepicker.min.js"></script>
	<script>
		$(document).ready(function() {

			$('.inputdatetbn').on('click', function() {
				$("#reservedate").datetimepicker('show');
			});

			$('.inputcidatetbn').on('click', function() {
				$("#reservecidate").datetimepicker('show');
			});

			$("#reservedate").datetimepicker({
				format: 'yyyy-mm-dd HH:ii p',
				showMeridian: true,
				autoclose: true,
				todayBtn: true
			});

			$("#reservecidate").datetimepicker({
				format: 'yyyy-mm-dd HH:ii p',
				showMeridian: true,
				autoclose: true,
				todayBtn: true
			});

			$('#confirm-delete').on('show.bs.modal', function(e) {
				console.log(this);

				$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
			});

			$('[data-toggle="popover"]').popover({
				container: 'body'
			});
		});
	</script>

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-46970702-2', 'auto');
		ga('send', 'pageview');
	</script>
	</body>

	</html>