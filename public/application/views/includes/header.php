<!doctype html>
<html class="no-js" lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>WTNH Studio 8 | Equipment Sign Out</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/bootstrap-datetimepicker.min.css">

	<link rel="apple-touch-icon" href="apple-touch-icon.png">
</head>

<body>
	<div class="container">
		<div class="row top-row">
			<?php $url = strtok($_SERVER["REQUEST_URI"], '/'); ?>

			<div class="col-md-12">
				<div class="header">
					<div class="btn-group pull-right" id="menu-bar">
						<a href="<?php echo base_url(); ?>index.php/status" id="report-btn" class="btn btn-default" rel="popover" data-content="" <?php echo ($url == 'report') ? 'disabled' : ''; ?>>Status</a>
						<a href="<?php echo base_url(); ?>index.php/equipment" id="equipment-btn" class="btn btn-default" rel="popover" data-content="" <?php echo ($url == 'equipment') ? 'disabled' : ''; ?>>Equipment</a>
						<a href="<?php echo base_url(); ?>index.php/vehicles" id="vehicles-btn" class="btn btn-default" rel="popover" data-content="" <?php echo ($url == 'vehicles') ? 'disabled' : ''; ?>>Vehicles</a>
						<a href="http://support.wtnh.com/" id="support-btn" class="btn btn-default" rel="popover" data-content="" target="_blank">Support</a>
						<a href="<?php echo base_url(); ?>index.php/admin" id="admin-btn" class="btn btn-default" rel="popover" data-content="" <?php echo ($url == 'admin') ? 'disabled' : ''; ?>>Admin</a>

					</div>
					<h3><span class="studio8-header">Studio 8</span> Equipment & Vehicles Request</h3>
				</div>
			</div>

		</div>