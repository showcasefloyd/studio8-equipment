	<div class="row">
		<br>
		<div class="col-sm-offset-1 col-md-10 well">

			<legend>Edit Vehicle Request</legend>
			<?php
			$attributes = array("class" => "form-horizontal", "id" => "vehicleForm", "name" => "vehicleForm");
			echo form_open("vehicles/edit/" . $record[0]->pkey_vehicle_reserve_id, $attributes);
			?>

			<fieldset>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="department" class="control-label">Reporter / Photographer</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "mmj"';
							echo form_dropdown('mmj_id', $mmjs, $record[0]->mmj_id, $attributes);
							?>
							<span class="text-danger"><?php echo form_error('mmj_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="reservedate" class="control-label">Dates</label>
						</div>

						<div class="col-md-4 col-sm-4">
							<div class="input-group">

								<input id="reservedate" name="reservedate" placeholder="Check out" type="text" class="form-control" value="<?php echo date('Y-m-d h:i a', strtotime($record[0]->date)); ?>" readonly>
								<span class="input-group-btn">
									<button class="btn btn-default inputdatetbn" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
								</span>

							</div>
						</div>

						<div class="col-md-4 col-sm-4">
							<div class="input-group">
								<input id="reservecidate" name="reservecidate" placeholder="Check in" type="text" class="form-control" value="<?php echo date('Y-m-d h:i a', strtotime($record[0]->returndate)); ?>" readonly>
								<span class="input-group-btn">
									<button class="btn btn-default inputcidatetbn" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
								</span>

							</div>

						</div>
						<div class="col-sm-offset-3 col-sm-8">
							<span class="text-danger"><?php echo form_error('reservedate'); ?></span>
							<?php echo $this->session->flashdata('errmsg'); ?>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">

						<div class="col-sm-offset-1 col-md-2">
							<label for="vehicle_id" class="control-label">Vehicle</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							if ($record[0]->available == 1 or $record[0]->available == 2 or $record[0]->archive == 1) {
								echo "<span class='text-danger'> <b>This vehicle (" . $record[0]->vehicle . ") is no longer available. Please select another. </b></span>";
							}

							$attributes = 'class = "form-control" id = "vehicle_id"';
							echo form_dropdown('vehicle_id', $vehicles, $record[0]->vehicle_id, $attributes);
							?>
							<span class="text-danger"><?php echo form_error('vehicle_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="notes" class="control-label">Notes</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<textarea id="notes" name="notes" class="form-control" rows="3"><?php echo $record[0]->notes; ?></textarea>
							<span class="text-danger"><?php echo form_error('notes'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
						<input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Edit"> <a href="<?php echo base_url(); ?>index.php/vehicles" id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</fieldset><?php echo form_close(); ?><?php echo $this->session->flashdata('msg'); ?>
		</div>