	<div class="row">
		<div class="col-md-12">
			<legend>Status Report <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></legend>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 ">
			<div class="admin-tool-bucket status-panel-large">
				<legend>Equipment (In Use)</legend>

				<ul class="clearfix">
					<li class="rowheader">Name</li>
					<li class="rowheader">Equipment</li>
					<li class="rowheader">Vehicle</li>
					<li class="rowheader">Reserve Date</li>
					<li class="rowheader">Return Date</li>
				</ul>

				<?php foreach ($equipRlist as $rl) : ?>
					<ul class="clearfix">
						<li><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> <?php echo $rl->first . " " . $rl->last; ?></li>

						<li><?php echo $rl->pack; ?></li>
						<li><?php echo (!empty($rl->vehicle)) ? $rl->vehicle : '<i>none</i>'; ?></li>
						<?php
						$dt = strtotime($rl->date);
						echo '<li ><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $dt) . '</li>';
						?>
						<?php
						$rdt = strtotime($rl->returndate);
						$late = (date("Y-m-d H:i") > $rl->returndate) ? 'unavailable' : 'available';
						echo '<li class="' . $late . '"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $rdt) . '</li>';
						?>

					</ul>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 ">
			<div class="admin-tool-bucket status-panel-large">
				<legend>Vehicles (In Use)</legend>

				<ul>
					<li class="rowheader">Name</li>
					<li class="rowheader">Equipment</li>
					<li class="rowheader">Vehicle</li>
					<li class="rowheader">Reserve Date</li>
					<li class="rowheader">Return Date</li>
				</ul>
				<ul>
					<?php foreach ($equipVlist as $rl) : ?>
						<li><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span><?php echo $rl->username; ?></li>
						<li><i>none</i></li>
						<li><?php echo (!empty($rl->vehicle)) ? $rl->vehicle : '<i>none</i>'; ?></li>

						<?php
						$dt = strtotime($rl->date);
						echo '<li><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $dt) . '</li>';
						$rdt = strtotime($rl->returndate);
						$late = (date("Y-m-d H:i") > $rl->returndate) ? 'unavailable' : 'available';
						echo '<li class="' . $late . '"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $rdt) . '</li>';
						?>

					<?php endforeach; ?>
				</ul>
			</div>
		</div>
	</div>

	<div class="row flex-row">
		<div class="status-grid">
			<div class="admin-tool-bucket status-panel">
				<legend>Available Equipment</legend>

				<?php foreach ($equiplist as $eq) : ?>
					<ul>
						<li class="pack"><?php echo $eq->pack; ?></li>

						<?php
						if (in_array($eq->pack, $availableEquipList)) {
							echo '<li class="available">Available</li>';
						} else if ($eq->available == 0) {
							echo '<li class="unavailable">In Use</li>';
						} else {
							echo '<li class="status">' . $packs_status[$eq->available] . '</li>';
						}
						?>

					</ul>
				<?php endforeach; ?>

			</div>
		</div>
		<div class="status-grid">
			<div class="admin-tool-bucket status-panel clearfix">
				<legend>Available Vehicles</legend>

				<?php foreach ($vehicles as $v) : ?>
					<ul>
						<li class="pack"><?php echo $v->vehicle; ?> </li>
						<?php
						if (in_array($v->vehicle, $availableVehicleList)) {
							echo '<li class="available">Available</li>';
						} else if ($v->available == 0) {
							echo '<li class="unavailable">In Use</li>';
						} else {
							echo '<li class="status">' . $vehicle_status[$v->available] . '</li>';
						}
						?>
					</ul>
				<?php endforeach; ?>


			</div>
		</div>

		<div class="status-grid">
			<div class="admin-tool-bucket status-panel clearfix">
				<legend>Out of Service Equipment</legend>

				<?php foreach ($equiplist as $eq) : ?>
					<ul>
						<li><?php echo $eq->pack; ?></li>
						<li <?php echo ($eq->available == 0) ? 'class="available"' : 'class="status"'; ?>>
							<?php echo $packs_status[$eq->available]; ?>
						</li>
					</ul>
				<?php endforeach; ?>

			</div>
		</div>
		<div class="status-grid">
			<div class="admin-tool-bucket status-panel clearfix">
				<legend>Out of Service Vehicles</legend>

				<?php foreach ($vehicles as $v) : ?>
					<ul>
						<li><?php echo $v->vehicle; ?> </li>
						<li <?php echo ($v->available == 0) ? 'class="available"' : 'class="status"'; ?>>
							<?php echo $vehicle_status[$v->available]; ?>
						</li>
					</ul>
				<?php endforeach; ?>


			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php echo $this->session->flashdata('msg'); ?>
		</div>
	</div>