	<div class="row">
		<br>
		<div class="col-sm-offset-1 col-md-10 well">

			<legend>Checkin Equipment</legend>
			<?php
			$attributes = array("class" => "form-horizontal", "id" => "equipForm", "name" => "equipForm");
			echo form_open("equipment/checkin/" . $record[0]->pkey, $attributes);
			?>

			<fieldset>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="department" class="control-label">Reporter / Photographer</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "mmj_id"';
							echo form_dropdown('mmj_id', $mmjs, $record[0]->mmj_id, $attributes);
							?>
							<span class="text-danger"><?php echo form_error('mmj_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label å class="control-label">Dates</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<label class="control-label">Current Check in time: <?php echo date("Y-m-d H:i a"); ?></label>
						</div>

					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="equipment" class="control-label">Equipment</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							//if($record[0]->available == 1 or $record[0]->archive == 1){
							echo "<b>Returning " . $record[0]->pack . " Pack </b>";
							//}  else {   

							//$attributes = 'class = "form-control" id = "vehicle_id"';
							//echo form_dropdown('vehicle_id', $vehicles, $record[0]->vehicle_id, $attributes); 

							//$attributes = 'class = "form-control" id = "equipment"';
							//echo form_dropdown('equipment', $equipment, $record[0]->equip_list_id, $attributes); 
							?>
							<span class="text-danger"><?php echo form_error('equipment'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-10">
							<p> Use this to reserve a News8 vehicle</p>
						</div>
						<div class="col-sm-offset-1 col-md-2">
							<label for="vehicle" class="control-label">Vehicles</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "vehicles"';
							echo form_dropdown('vehicle_id', $vehicles, $record[0]->vehicle_id, $attributes);
							?>
							<span class="text-danger"><?php echo form_error('vehicle_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="notes" class="control-label">Notes</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<textarea id="notes" name="notes" class="form-control" rows="3"><?php echo $record[0]->notes; ?></textarea>
							<span class="text-danger"><?php echo form_error('notes'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
						<input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Check In">

						<a href="<?php echo base_url(); ?>index.php/equipment" id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</fieldset><?php echo form_close(); ?><?php echo $this->session->flashdata('msg'); ?>
		</div>