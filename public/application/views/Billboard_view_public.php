<!doctype html>
<html class="no-js" lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>WTNH Studio 8 | Equipment Sign Out</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/bootstrap.min.css">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
</head>

<body>
	<div class="container billboard">

		<div class="row">
			<div class="col-md-12">
				<legend><span class="glyphicon glyphicon-bullhorn" aria-hidden="true"></span> <span class="studio8-header">Studio 8</span> Equipment & Vehicles Request <p class='pull-right billboard-time'><?php echo "Status as of: " . date('l F, jS Y -- h:i a'); ?>
				</legend>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 ">
				<div class="billboard-tool-bucket status-panel-billboard">
					<h3>Equipment (In Use)</h3>

					<?php foreach ($equipRlist as $rl) : ?>
						<ul class="clearfix bb">
							<li class="bb-name"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span> <?php echo $rl->first . " " . $rl->last;  ?> / <?php echo $rl->pack; ?> <?php echo (!empty($rl->vehicle)) ? ' / Vehicle: ' . $rl->vehicle : ''; ?></li>
							<?php
							$dt = strtotime($rl->date);
							echo '<li class="bb-date"> Out: ' . date('m/j/y g:ia', $dt) . '</li>';
							?>
							<?php
							$rdt = strtotime($rl->returndate);
							$late = (date("Y-m-d H:i") > $rl->returndate) ? 'unavailable' : 'available';
							echo '<li class="bb-date ' . $late . '"> Due: ' . date('m/j/y g:ia', $rdt) . '</li>';
							?>
						</ul>
					<?php endforeach; ?>
				</div>
			</div>

			<div class="col-md-4 ">
				<div class="billboard-tool-bucket status-panel-billboard">
					<h3>Vehicles (In Use)</h3>

					<?php foreach ($equipVlist as $rl) : ?>
						<ul class="clearfix bb">
							<li class="bb-name"><span class="carpng"></span> <?php echo $rl->username; ?> / <?php echo (!empty($rl->vehicle)) ? $rl->vehicle : '<i>missing</i>'; ?></li>
							<?php
							$dt = strtotime($rl->date);
							echo '<li class="bb-date"> Out: ' . date('m/j/y g:ia', $dt) . '</li>';
							$rdt = strtotime($rl->returndate);
							$late = (date("Y-m-d H:i") > $rl->returndate) ? 'unavailable' : 'available';
							echo '<li class="bb-date ' . $late . '"> Due: ' . date('m/j/y g:ia', $rdt) . '</li>';
							?>
						</ul>
					<?php endforeach; ?>

				</div>
			</div>

			<div class="col-md-4">
				<div class="billboard-tool-bucket status-panel-billboard">
					<h3>Available Equipment</h3>

					<?php foreach ($equiplist as $eq) : ?>
						<ul class="bb">
							<li class="bb-name"><?php echo $eq->pack; ?></li>

							<?php
							if (in_array($eq->pack, $availableEquipList)) {
								echo '<li class="bb-pack available">Available</li>';
							} else if ($eq->available == 0) {
								echo '<li class="bb-pack unavailable">In Use</li>';
							} else {
								echo '<li class="bb-pack status">' . $packs_status[$eq->available] . '</li>';
							}
							?>

						</ul>
					<?php endforeach; ?>

				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="billboard-tool-bucket status-panel-billboard">
					<h3>Available Vehicles</h3>

					<?php foreach ($vehicles as $v) : ?>
						<ul class="bb-full">
							<li class="bb-name"><?php echo $v->vehicle; ?> </li>

							<?php
							if (in_array($v->vehicle, $availableVehicleList)) {
								echo '<li class="bb-data available">Available</li>';
							} else if ($v->available == 0) {
								echo '<li class="bb-data unavailable">In Use</li>';
							} else {
								echo '<li class="bb-data status">' . $vehicle_status[$v->available] . '</li>';
							}
							?>
						</ul>
					<?php endforeach; ?>


				</div>

			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="footer">
					<p>&copy; WTNH <?php echo date("Y"); ?></p>
				</div>
			</div>
		</div>

	</div><!-- End Container -->


	<script src="<?php echo base_url(); ?>/scripts/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>/scripts/bootstrap.min.js"></script>
	<script>
		$(document).ready(function() {

		});
	</script>

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
</body>

</html>