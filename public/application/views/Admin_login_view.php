	<div class="row">
		<div class="col-md-12">
			<legend>Manage Database Assets (Login Required) <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span></legend>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">

			<?php
			$attributes = array("class" => "form-horizontal", "id" => "verifyForm", "name" => "verifyForm");
			echo form_open("verify", $attributes);
			?>

			<div class="form-group">

				<label for="username" class="col-sm-offset-1 col-sm-2 control-label">Username:</label>
				<div class="col-sm-6">
					<input type="text" size="20" id="username" name="username" class="form-control" />
					<span class="text-danger"><?php echo form_error('username'); ?></span>
				</div>
			</div>

			<div class="form-group">
				<label for="password" class="col-sm-offset-1 col-sm-2 control-label">Password:</label>
				<div class="col-sm-6">
					<input type="password" size="20" id="password" name="password" class="form-control" />
					<span class="text-danger"><?php echo form_error('password'); ?></span>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-6">
					<button type="submit" class="btn btn-primary">Login</button>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<?php echo $this->session->flashdata('msg'); ?>
			</div>
		</div>