	<div class="row">
		<br>
		<div class="col-sm-offset-1 col-md-10 well">

			<legend>Check In Vehicles</legend>
			<?php
			$attributes = array("class" => "form-horizontal", "id" => "vehicleForm", "name" => "vehicleForm");
			echo form_open("vehicles/checkin/" . $record[0]->pkey_vehicle_reserve_id, $attributes);
			?>

			<fieldset>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="department" class="control-label">Reporter / Photographer</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "mmj_id"';
							echo form_dropdown('mmj_id', $mmjs, $record[0]->mmj_id, $attributes);
							?>
							<span class="text-danger"><?php echo form_error('mmj_id'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label å class="control-label">Dates</label>
						</div>

						<div class="col-md-8 col-sm-8">
							Check in date/time: <?php echo date("Y-m-d H:i a"); ?>
						</div>

					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">

						<div class="col-sm-offset-1 col-md-2">
							<label for="vehicle_id" class="control-label">Vehicle</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							echo "Returning vehicle: " . $record[0]->vehicle;
							?>
							<span class="text-danger"><?php echo form_error('vehicle_id'); ?></span>
						</div>
					</div>
				</div>


				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="odometer" class="control-label">Odometer</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<input id="odometer" name="odometer" placeholder="What is the ending odometer?" type="text" class="form-control" value="<?php echo set_value('odometer'); ?>"> <span class="text-danger"><?php echo form_error('odometer'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="notes" class="control-label">Notes</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<textarea id="notes" name="notes" class="form-control" rows="3"><?php echo $record[0]->notes; ?></textarea>
							<span class="text-danger"><?php echo form_error('notes'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
						<input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Checkin"> <a href="<?php echo base_url(); ?>index.php/vehicles" id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</fieldset><?php echo form_close(); ?><?php echo $this->session->flashdata('msg'); ?>
		</div>