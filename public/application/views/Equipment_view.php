	<div class="row">
		<div class="col-md-12 ">
			<div class="admin-tool-bucket equipment-admin">

				<legend>Check Out Equipment <span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span><a href="<?php echo base_url(); ?>index.php/equipment/checkout"><button type="button" class="btn btn-primary">Checkout Equipment</button></a></legend>

				<div class="col-md-2 rowheader">Name</div>
				<div class="col-md-2 rowheader">Equipment</div>
				<div class="col-md-2 rowheader">Vehicle</div>
				<div class="col-md-2 rowheader">Reserve Date</div>
				<div class="col-md-2 rowheader">Return Date</div>
				<div class="col-md-2 rowheader">Action</div>
				<div class="clearrow"></div>

				<?php
				foreach ($clist as $cl) : ?>

					<div class="col-md-2 name"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> <?php echo $cl->first . " " . $cl->last; ?></div>
					<div class="col-md-2 data"><?php echo $cl->pack; ?></div>
					<div class="col-md-2 data"><?php echo (!empty($cl->vehicle)) ? $cl->vehicle : '<i>none</i>'; ?></div>
					<?php
					$cdt = strtotime($cl->date);
					echo '<div class="col-md-2 date"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $cdt) . '</div>';
					$cdt = strtotime($cl->returndate);
					$late = (date("Y-m-d H:i") > $cl->returndate) ? 'unavailable' : 'available';
					echo '<div class="col-md-2 date ' . $late . '"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $cdt) . '</div>';
					?>
					<div class="col-md-2 data"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>
						<?php echo "<a href=" . base_url() . "index.php/equipment/checkin/" . $cl->pkey . ">Check In</a> | <span class=\"glyphicon glyphicon-erase\" aria-hidden=\"true\"></span>  <a href='#' data-href='" . base_url() . "index.php/equipment/cancel/" . $cl->pkey . "' class='confirm' data-toggle='modal' data-target='#confirm-delete'>Cancel</a>";  ?>
					</div>
					<div class="clearrow"></div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 ">
			<div class="admin-tool-bucket equipment-admin">
				<legend>Reserved Equipment </legend>

				<div class="col-md-2 rowheader">Name</div>
				<div class="col-md-2 rowheader">Equipment</div>
				<div class="col-md-2 rowheader">Vehicle</div>
				<div class="col-md-2 rowheader">Reserve Date</div>
				<div class="col-md-2 rowheader">Return Date</div>
				<div class="col-md-2 rowheader">Action</div>

				<?php foreach ($rlist as $rl) : ?>

					<div class="col-md-2 name"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> <?php echo $rl->first . " " . $rl->last; ?></div>
					<div class="col-md-2 data"><?php echo $rl->pack; ?></div>
					<div class="col-md-2 data"> <?php echo (!empty($rl->vehicle)) ? $rl->vehicle : '<i>none</i>'; ?></div>
					<?php
					$dt = strtotime($rl->date);
					echo '<div class="col-md-2 date"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $dt) . '</div>';
					$rdt = strtotime($rl->returndate);
					echo '<div class="col-md-2 date"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $rdt) . '</div>';
					?>
					<div class="col-md-2 data"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
						<?php echo "<a href=" . base_url() . "index.php/equipment/edit/" . $rl->pkey . ">Edit</a> | <span class=\"glyphicon glyphicon-erase\" aria-hidden=\"true\"></span>  <a href='#' data-href='" . base_url() . "index.php/equipment/cancel/" . $rl->pkey . "' class='confirm' data-toggle='modal' data-target='#confirm-delete'>Cancel</a>";  ?>
					</div>
					<div class="clearrow"></div>

				<?php endforeach; ?>
			</div>

		</div>
	</div>
	<div class="row">
		<div class="col-md-12 ">
			<div class="admin-tool-bucket equipment-admin">
				<legend>History: Last 10 Returned </legend>

				<div class="col-md-2 rowheader">Name</div>
				<div class="col-md-2 rowheader">Equipment</div>
				<div class="col-md-2 rowheader">Vehicle</div>
				<div class="col-md-1 rowheader">Notes</div>
				<div class="col-md-3 rowheader">Reserve Date</div>
				<div class="col-md-2 rowheader">Checked In Date</div>
				<div class="clearrow"></div>

				<?php foreach ($hlist as $hl) : ?>
					<div class="col-md-2 name"><span class="glyphicon glyphicon-sunglasses" aria-hidden="true"></span> <?php echo $hl->first . " " . $hl->last; ?></div>
					<div class="col-md-2 data"><?php echo $hl->pack; ?></div>
					<div class="col-md-2 data"> <?php echo (!empty($hl->vehicle)) ? $hl->vehicle : '<i>none</i>'; ?></div>
					<div class="col-md-1 data">
						<?php if (!empty($hl->notes)) { ?>
							<button type="button" class="btn btn-xs btn-warning" data-placement="bottom" data-toggle="popover" data-content="<?php echo $hl->notes; ?>">Notes</button>
						<?php } ?>
					</div>
					<?php
					$dt = strtotime($hl->date);
					echo '<div class="col-md-3 date"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $dt) . '</div>';
					$hdt = strtotime($hl->checkedInTime);
					echo '<div class="col-md-2 date"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> ' . date('M jS, Y  g:ia', $hdt) . '</div>';
					?>
					<div class="clearrow"></div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php echo $this->session->flashdata('msg'); ?>
		</div>
	</div>