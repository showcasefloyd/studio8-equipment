	<div class="row">
		<br>
		<div class="col-sm-offset-1 col-md-10 well">

			<legend>Add MMJ Details</legend>
			<?php
			$attributes = array("class" => "form-horizontal", "id" => "mmjform", "name" => "mmjform");
			echo form_open("mmjs/add", $attributes);
			?>

			<fieldset>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="firstname" class="control-label">First Name</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<input id="firstname" name="firstname" placeholder="First name" type="text" class="form-control" value="<?php echo set_value('firstname'); ?>">
							<span class="text-danger"><?php echo form_error('firstname'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="lastname" class="control-label">Last Name</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<input id="lastname" name="lastname" placeholder="Last name" type="text" class="form-control" value="<?php echo set_value('lastname'); ?>">
							<span class="text-danger"><?php echo form_error('lastname'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="email" class="control-label">Email</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<input id="email" name="email" placeholder="Email Address" type="text" class="form-control" value="<?php echo set_value('email'); ?>">
							<span class="text-danger"><?php echo form_error('email'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row colbox">
						<div class="col-sm-offset-1 col-md-2">
							<label for="department" class="control-label">Department</label>
						</div>

						<div class="col-md-8 col-sm-8">
							<?php
							$attributes = 'class = "form-control" id = "department"';
							echo form_dropdown('department', $department, set_value('department'), $attributes); ?>
							<span class="text-danger"><?php echo form_error('department'); ?></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-4 col-lg-8 col-sm-8 text-left">
						<input id="btn_add" name="btn_add" type="submit" class="btn btn-primary" value="Insert"> <a href="<?php echo base_url(); ?>index.php/admin" id="btn_cancel" name="btn_cancel" type="reset" class="btn btn-danger">Cancel</a>
					</div>
				</div>
			</fieldset><?php echo form_close(); ?><?php echo $this->session->flashdata('msg'); ?>
		</div>