<?php
class Vehicles_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_vehicles_list($select = true)
	{
		$vehi = array();
		if ($select) {
			$vehi[0] = '-- SELECT --';
		}
		$this->db->select("*");
		$this->db->from('equip_vehicles');
		$this->db->where('available = 0');
		$this->db->where('archive = 0');
		$query = $this->db->get();
		$data = $query->result_array();
		foreach ($data as $ve) {

			$vehi[$ve['pkey_vehicle_id']] = $ve['vehicle'];
		}

		return $vehi;
	}

	public function get_full_vehicles_list()
	{
		$this->db->select("*");
		$this->db->from('equip_vehicles');
		$this->db->where('archive = 0');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_reserved_vehicles_list()
	{
		$this->db->select('*');
		$this->db->from('equip_vehicle_reserve as R');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id');
		$this->db->where('R.date >= concat(CURDATE()," ",CURTIME())');
		$this->db->where('R.checkedin = 0');
		$this->db->order_by("R.date", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_checkedout_vehicles_list()
	{

		$this->db->select('*');
		$this->db->from('newsroom-reservations-view as R');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.date < concat(CURDATE()," ",CURTIME())');
		$this->db->where('R.ve_reserve_id != ""');
		$this->db->order_by("R.returndate", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_late_vehicles_report()
	{
		$this->db->select('*');
		$this->db->from('equip_vehicle_reserve as R');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.date < concat(CURDATE()," ",CURTIME())');
		$this->db->where('concat(CURDATE()," ",CURTIME())  >=  R.returndate');
		$this->db->where('R.checkedin = 0');
		$this->db->order_by("R.returndate", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_vehicles_history()
	{
		$this->db->select('*');
		$this->db->from('equip_vehicle_reserve as R');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.checkedin = 1');
		$this->db->order_by("R.checkedInTime", "desc");
		$this->db->limit(10);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_vehicle_reservation($pkey)
	{

		$this->db->select('*');
		$this->db->from('equip_vehicle_reserve as R');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.pkey_vehicle_reserve_id = ' . $pkey);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_vehicle($pkey)
	{

		$this->db->select("*");
		$this->db->from("equip_vehicles");
		$this->db->where("pkey_vehicle_id =" . $pkey);
		$query = $this->db->get();
		return $query->result();
	}

	/* @reserve_id checks for conflicts */
	public function checkForReserve($startdate, $enddate, $vehicle_id, $reserve_id = 0)
	{

		$st = date("Y-m-d H:i", strtotime($startdate));
		$en = date("Y-m-d H:i", strtotime($enddate));

		$sql  =  "SELECT * FROM `equip_vehicle_reserve` WHERE (? between `date` AND `returndate` ";
		$sql .= " OR `date` BETWEEN ? AND ?  ";
		$sql .= " OR `returndate` BETWEEN ? AND ?) and (`vehicle_id` = ?) and (`checkedin` = 0)";

		$query = $this->db->query(
			$sql,
			array($st, $st, $en, $st, $en, $vehicle_id)
		);
		$row = $query->result();

		if (!empty($row)) {
			/* Check to see if we are editing our own record. If we are, then remove it */
			$arrayCount = 0;
			foreach ($row as $k => $v) {
				if ($v->pkey_vehicle_reserve_id == $reserve_id) {
					unset($row[$arrayCount]);
					$arrayCount++;
				}
			}

			// If $noKey is true, then return error
			if (!empty($row)) {
				return $this->get_vehicle_reservation($row[0]->pkey_vehicle_reserve_id);
			}
		}
	}

	public function get_vehicles_available_now()
	{
		$veList = array();

		$this->db->select('R.vehicle_id');
		$this->db->from('newsroom-reservations-view as R');
		$this->db->where('R.date < concat(CURDATE()," ",CURTIME())');
		$this->db->order_by("R.returndate", "asc");

		$subquery = $this->db->get_compiled_select();

		$this->db->select("*");
		$this->db->from("equip_vehicles");
		$this->db->where("archive = 0");
		$this->db->where("available = 0");
		$this->db->where("pkey_vehicle_id NOT IN ($subquery)", NULL, FALSE);

		$query = $this->db->get();

		$data = $query->result_array();
		foreach ($data as $ve) {
			$veList[$ve['pkey_vehicle_id']] = $ve['vehicle'];
		}

		return $veList;
	}

	public function grab_user_details($id)
	{
		$this->db->select('*');
		$this->db->from('equip_mmjs');
		$this->db->where('pkey = ' . $id);
		$query = $this->db->get();

		return $data = $query->result();
	}
}
