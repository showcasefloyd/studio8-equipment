<?php
class Mmjs_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_mmjs($select = true)
	{
		$mmjlist = array();
		if ($select) {
			$mmjlist[0] = '-- SELECT --';
		}

		$query = $this->db->get('equip_mmjs');
		$mmjs = $query->result_array();
		foreach ($mmjs as $mj) {
			$mmjlist[$mj['pkey']] = $mj['first'] . ' ' . $mj['last'];
		}

		return $mmjlist;
	}

	public function get_full_mmjs()
	{

		$this->db->select("*");
		$this->db->from('equip_mmjs as M');
		$this->db->join('equip_department as D', 'M.department_id = D.department_id', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_departments()
	{
		$this->db->select('department_id');
		$this->db->select('department_name');
		$this->db->from('equip_department');
		$query = $this->db->get();
		$result = $query->result();

		// array to store department id & department name
		$dept_id = array('-SELECT-');
		$dept_name = array('-SELECT-');

		for ($i = 0; $i < count($result); $i++) {
			array_push($dept_id, $result[$i]->department_id);
			array_push($dept_name, $result[$i]->department_name);
		}
		return $department_result = array_combine($dept_id, $dept_name);
	}

	// fetch employee record by employee no
	function get_mmj_record($empno)
	{
		$this->db->where('pkey', $empno);
		$this->db->from('equip_mmjs');
		$query = $this->db->get();
		return $query->result();
	}
}
