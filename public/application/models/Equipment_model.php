<?php
class Equipment_model extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_equipment_list($select = true, $available = true)
	{
		$equilist = array();
		if ($select) {
			$equilist[0] = '-- SELECT --';
		}
		$this->db->select("*");
		$this->db->from('equip_list');
		$this->db->where('available = 0');
		$this->db->where('archive = 0');
		$query = $this->db->get();
		$equi = $query->result_array();
		foreach ($equi as $eq) {
			$equilist[$eq['pkey']] = $eq['pack'];
		}
		return $equilist;
	}


	public function get_full_equipment_list()
	{
		$this->db->select("*");
		$this->db->from('equip_list');
		$this->db->where('archive = 0');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_reserve_list()
	{
		$this->db->select('R.pkey,R.notes,R.date,R.returndate,V.vehicle,M.first,M.last,L.pack,L.description, L.archive, L.available, M.pkey as mmj_id', false);
		$this->db->from('equip_reserve as R');
		$this->db->join('equip_mmjs as M', 'R.mmj_id = M.pkey');
		$this->db->join('equip_list as L', 'R.equip_list_id = L.pkey', 'left');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.date >= concat(CURDATE()," ",CURTIME())');
		$this->db->where('R.checkedin = 0');
		$this->db->order_by("R.date", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_checkedout_list()
	{
		$this->db->select('R.pkey, R.notes,R.date,R.returndate,V.vehicle,M.first,M.last,L.pack,L.description, L.archive, L.available, M.pkey as mmj_id', false);
		$this->db->from('equip_reserve as R');
		$this->db->join('equip_mmjs as M', 'R.mmj_id = M.pkey');
		$this->db->join('equip_list as L', 'R.equip_list_id = L.pkey', 'left');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.date <= concat(CURDATE()," ",CURTIME())');
		$this->db->where('R.checkedin = 0');
		$this->db->order_by("R.returndate", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_late_equipment_report()
	{
		$this->db->select('R.pkey, R.notes,R.date,R.returndate,V.vehicle,M.first,M.last,M.email,L.pack,L.description, L.archive, L.available, M.pkey as mmj_id', false);
		$this->db->from('equip_reserve as R');
		$this->db->join('equip_mmjs as M', 'R.mmj_id = M.pkey');
		$this->db->join('equip_list as L', 'R.equip_list_id = L.pkey', 'left');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.date <= concat(CURDATE()," ",CURTIME())');
		$this->db->where('concat(CURDATE()," ",CURTIME())  >=  R.returndate');
		$this->db->where('R.checkedin = 0');
		$this->db->order_by("R.returndate", "desc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_checkedout_history()
	{
		$this->db->select('R.pkey,R.notes,R.date,R.returndate, R.checkedInTime,V.vehicle,M.first,M.last,L.pack,L.description, L.archive, L.available, M.pkey as mmj_id', false);
		$this->db->from('equip_reserve as R');
		$this->db->join('equip_mmjs as M', 'R.mmj_id = M.pkey', 'left');
		$this->db->join('equip_list as L', 'R.equip_list_id = L.pkey', 'left');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.checkedin = 1');
		$this->db->order_by("R.checkedInTime", "desc");
		$this->db->limit(10);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_equipment($pkey)
	{
		$this->db->select('R.pkey,R.mmj_id,R.equip_list_id,R.vehicle_id,R.notes,R.date,R.returndate,M.first, M.last,L.pack,L.pkey as equip_id, L.archive, L.available, concat(M.first," ",M.last) as user, V.vehicle');
		$this->db->from('equip_reserve as R');
		$this->db->join('equip_mmjs as M', 'R.mmj_id = M.pkey', 'left');
		$this->db->join('equip_list as L', 'R.equip_list_id = L.pkey', 'left');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.pkey = ' . $pkey);
		$this->db->order_by("R.date", "asc");
		$query = $this->db->get();
		return $query->result();
	}

	public function get_pack($pkey)
	{

		$this->db->select("*");
		$this->db->from("equip_list");
		$this->db->where("pkey =" . $pkey);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @reserve_id checks for conflicts 
	 * */
	public function checkForReserve($startdate, $enddate, $equip_list_id, $vehicle_id = 0, $reserve_id = 0)
	{

		$st = date("Y-m-d H:i", strtotime($startdate));
		$en = date("Y-m-d H:i", strtotime($enddate));

		$sql  =  "SELECT * FROM `equip_reserve` WHERE (? between `date` AND `returndate` ";
		$sql .= " OR `date` BETWEEN ? AND ?  ";
		$sql .= " OR `returndate` BETWEEN ? AND ?) and (`equip_list_id` = ? ";
		if ($vehicle_id != 0) {
			$sql .= "OR `vehicle_id` = " . $vehicle_id . " ";
		}
		$sql .= ") and (`checkedin` = 0)";

		$query = $this->db->query(
			$sql,
			array($st, $st, $en, $st, $en, $equip_list_id)
		);
		$row = $query->result();

		if (!empty($row)) {
			/* Check to see if we are editing our own record. If we are then remove it */
			$arrayCount = 0;
			foreach ($row as $k => $v) {
				if ($v->pkey == $reserve_id) {
					unset($row[$arrayCount]);
				}
				$arrayCount++;
			}
			if (!empty($row)) {
				return $this->get_equipment($row[0]->pkey);
			}
		}
	}

	/**
	 *	@Startdate = When the Vehicle is going to be check out
	 *	@Vehicle_id = Check the equipment table to see if any vehicles are recorded
	 *	for this time
	 * */

	public function checkForVehicleConflicts($startdate, $vehicle_id)
	{

		$st = date("Y-m-d H:i", strtotime($startdate));

		$sql  =  "SELECT * FROM equip_reserve as E WHERE DATE(?) between DATE(E.date) ";
		$sql .=  "AND DATE(E.returndate) and E.checkedin = 0 and E.vehicle_id = ?";

		$query = $this->db->query(
			$sql,
			array($st, $vehicle_id)
		);
		$row = $query->result();
		if (!empty($row)) {
			return $this->get_equipment($row[0]->pkey);
		}
	}

	public function get_equipment_available_now()
	{
		$equilist = array();

		$this->db->select('R.equip_list_id');
		$this->db->from('equip_reserve as R');
		$this->db->join('equip_mmjs as M', 'R.mmj_id = M.pkey');
		$this->db->join('equip_list as L', 'R.equip_list_id = L.pkey', 'left');
		$this->db->join('equip_vehicles as V', 'R.vehicle_id = V.pkey_vehicle_id', 'left');
		$this->db->where('R.date <= concat(CURDATE()," ",CURTIME())');
		$this->db->where('R.checkedin = 0');
		$subquery = $this->db->get_compiled_select();


		$this->db->select("*");
		$this->db->from("equip_list");
		$this->db->where("archive = 0");
		$this->db->where("available = 0");
		$this->db->where("pkey NOT IN ($subquery)", NULL, FALSE);

		$query = $this->db->get();

		$data = $query->result_array();
		foreach ($data as $eq) {
			$equilist[$eq['pkey']] = $eq['pack'];
		}
		return $equilist;
	}
}
