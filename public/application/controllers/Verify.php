<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Verify extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		//$this->load->model('user','',TRUE);
	}

	function index()
	{
		//This method will have the credentials validation
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_validate_user');

		if ($this->form_validation->run() == FALSE) {
			$data['page'] = 'Admin_login_view';
			//Field validation failed.  User redirected to login page
			$this->load->view('includes/layout', $data);
		} else {
			//Go to private area
			redirect('admin', 'refresh');
		}
	}

	function validate_user()
	{
		$user = $this->input->post('username');
		$pass = $this->input->post('password');

		if ($user == 'admin' && $pass == 'wtnhwctx') {
			$sess_array = array();

			$sess_array = array(
				'username' => 'Admin'
			);
			$this->session->set_userdata('logged_in', $sess_array);

			return TRUE;
		} else {

			$this->form_validation->set_message('validate_user', 'Invalid username or password');

			return false;
		}
	}

	/* Not used but good fro reference */
	function check_database($password)
	{
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');

		//query the database
		$result = $this->user->login($username, $password);

		if ($result) {
			$sess_array = array();
			foreach ($result as $row) {
				$sess_array = array(
					'id' => $row->id,
					'username' => $row->username
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			return TRUE;
		} else {
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}


	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('verify', 'refresh');
	}
}
