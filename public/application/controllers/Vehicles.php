<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Vehicles extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('NewsroomFuncs');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('form_validation');

		$this->load->model('Vehicles_model');
		$this->load->model('Equipment_model');
		$this->load->model('Mmjs_model');
	}

	public function index()
	{
		$data['clist'] = $this->Vehicles_model->get_checkedout_vehicles_list();
		$data['rlist'] = $this->Vehicles_model->get_reserved_vehicles_list();
		$data['hlist'] = $this->Vehicles_model->get_vehicles_history();

		$data['page'] = 'Vehicles_view';
		$this->load->view('includes/layout', $data);
	}

	public function checkout()
	{
		$data['vehicles'] = $this->Vehicles_model->get_vehicles_list();
		$data['mmjs'] = $this->Mmjs_model->get_mmjs();

		$data['page'] = 'VehiclesReserve_view';

		$this->form_validation->set_rules('mmj_id', 'MMJ', 'callback_name_check');

		$this->form_validation->set_rules('reservedate', 'checkout reserve date', 'trim|required|callback_date_check[' . $this->input->post('reservecidate') . ']');
		$this->form_validation->set_rules('vehicle_id', 'vehicle_id', 'callback_vehicle_check');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('includes/layout', $data);
		} else {
			//check for previous Vehicles reservations
			$reserveCheck = $this->Vehicles_model->checkForReserve(
				$this->input->post('reservedate'),
				$this->input->post('reservecidate'),
				$this->input->post('vehicle_id')
			);

			//check for any reserved/checkedout Vehicles in the Equiqment table
			$checkVehicleConflicts = $this->Equipment_model->checkForVehicleConflicts(
				$this->input->post('reservedate'),
				$this->input->post('vehicle_id')
			);

			if (count($reserveCheck) > 0) {
				$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears this vehicle <b>" . $reserveCheck[0]->vehicle . "</b> is not available. It's currently reserved by <b>" .  $reserveCheck[0]->name . "</b> for this date and time already. It is due to be returned <b>" . $reserveCheck[0]->returndate . "</b></div>");

				$this->load->view('includes/layout', $data);
			} else if (count($checkVehicleConflicts) > 0) {
				$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears this vehicle has already been reserved or is in use. It's currently reserved by <b>" . $checkVehicleConflicts[0]->user . "</b> for this date and time already. It is due to be returned <b>" . $checkVehicleConflicts[0]->returndate . "</b></div>");

				$this->load->view('includes/layout', $data);
			} else {
				$userId = $this->input->post('mmj_id');
				$user = $this->Vehicles_model->grab_user_details($userId);

				//pass validation
				$data = array(
					'name' => $user[0]->first . ' ' . $user[0]->last,
					'email' => $user[0]->email,
					'mmj_id' => $userId,
					'notes' => $this->input->post('notes'),
					'date' => date("Y-m-d H:i", strtotime($this->input->post('reservedate'))),
					'returndate' => date("Y-m-d H:i", strtotime($this->input->post('reservecidate'))),
					'vehicle_id' => $this->input->post('vehicle_id')
				);
				$this->db->insert('equip_vehicle_reserve', $data);
				//display success message
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Vehicle has been reserved.</div>');

				redirect('vehicles');
			}
		}
	}

	public function edit($pkey)
	{
		$data['vehicles'] = $this->Vehicles_model->get_vehicles_list();
		$data['mmjs'] = $this->Mmjs_model->get_mmjs();
		$data['record'] = $this->Vehicles_model->get_vehicle_reservation($pkey);

		$data['page'] = 'VehiclesUpdate_view';

		// set validation rules
		$this->form_validation->set_rules('mmj_id', 'MMJ', 'callback_name_check');
		$this->form_validation->set_rules('reservedate', 'checkout reserve date', 'trim|required|callback_date_check[' . $this->input->post('reservecidate') . ']');
		$this->form_validation->set_rules('vehicle_id', 'vehicle_id', 'callback_vehicle_check');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('includes/layout', $data);
		} else {
			// check for previous reservations
			$reserveCheck = $this->Vehicles_model->checkForReserve(
				$this->input->post('reservedate'),
				$this->input->post('reservecidate'),
				$this->input->post('vehicle_id'),
				$data['record'][0]->pkey_vehicle_reserve_id
			);

			// check for any reserved/checkedout Vehicles in the Equiqment table
			$checkVehicleConflicts = $this->Equipment_model->checkForVehicleConflicts(
				$this->input->post('reservedate'),
				$this->input->post('vehicle_id')
			);

			if (count($reserveCheck) > 0) {
				$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears this vehicle <b>" . $reserveCheck[0]->vehicle . "</b> is not available. It's currently reserved by <b>" .  $reserveCheck[0]->name . "</b> for this date and time already. It is due to be returned <b>" . $reserveCheck[0]->returndate . "</b></div>");

				$this->load->view('includes/layout', $data);
			} else if (count($checkVehicleConflicts) > 0) {
				$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears this vehicle has already been reserved or is in use. It's currently reserved by <b>" . $checkVehicleConflicts[0]->user . "</b> for this date and time already. It is due to be returned <b>" . $checkVehicleConflicts[0]->returndate . "</b></div>");

				$this->load->view('includes/layout', $data);
			} else {
				$userId = $this->input->post('mmj_id');
				$user = $this->Vehicles_model->grab_user_details($userId);

				// pass validation
				$data = array(
					'name' => $user[0]->first . ' ' . $user[0]->last,
					'email' => $user[0]->email,
					'mmj_id' => $userId,
					'notes' => $this->input->post('notes'),
					'date' => date("Y-m-d H:i", strtotime($this->input->post('reservedate'))),
					'returndate' => date("Y-m-d H:i", strtotime($this->input->post('reservecidate'))),
					'vehicle_id' =>  $this->input->post('vehicle_id')
				);
				$this->db->where('pkey_vehicle_reserve_id', $pkey);
				$this->db->update('equip_vehicle_reserve', $data);

				// display success message
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Vehicle request has been updated.</div>');

				redirect('vehicles');
			}
		}
	}

	public function checkin($pkey)
	{
		$data['vehicles'] = $this->Vehicles_model->get_vehicles_list();
		$data['mmjs'] = $this->Mmjs_model->get_mmjs();
		$data['record'] = $this->Vehicles_model->get_vehicle_reservation($pkey);

		$data['page'] = 'VehiclesCheckin_view';

		//set validation rules
		$this->form_validation->set_rules('mmj_id', 'MMJ', 'callback_name_check');
		$this->form_validation->set_rules('vehicle_id', 'vehicle_id', 'callback_vehicle_check');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('includes/layout', $data);
		} else {
			$userId = $this->input->post('mmj_id');
			$user = $this->Vehicles_model->grab_user_details($userId);

			//pass validation
			$data = array(
				'name' => $user[0]->first . ' ' . $user[0]->last,
				'email' => $user[0]->email,
				'mmj_id' => $userId,
				'notes' => $this->input->post('notes'),
				'checkedin' => '1',
				'checkedInTime' => date("Y-m-d H:i:s")
			);
			$this->db->where('pkey_vehicle_reserve_id', $pkey);
			$this->db->update('equip_vehicle_reserve', $data);

			//display success message
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Vehicle has been checked in.</div>');

			redirect('vehicles');
		}
	}

	public function cancel($pkey)
	{
		$this->db->where('pkey_vehicle_reserve_id', $pkey);
		$this->db->delete('equip_vehicle_reserve');

		// display success message
		$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Your vehicle request has been cancelled.</div>');
		redirect('vehicles');
	}

	public function addVehicle()
	{
		$data['page'] = 'NewsVehicleAdd_view';
		$data['vehicle_status'] = $this->newsroomfuncs->get_vehicle_status();

		// set validation rules
		$this->form_validation->set_rules('vehicle', 'pack', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('includes/layout', $data);
		} else {
			// pass validation
			$data = array(
				'vehicle' => $this->input->post('vehicle'),
				'available' => $this->input->post('available'),
				'description' => $this->input->post('description'),
				'licplate' => $this->input->post('licplate')
			);

			$this->db->insert('equip_vehicles', $data);

			// display success message
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">New vehicle has been added to the database.</div>');

			redirect('admin');
		}
	}

	public function updateVehicle($pkey)
	{

		$this->load->model('Vehicles_model');
		$data['vehicledata'] = $this->Vehicles_model->get_vehicle($pkey);
		$data['vehicle_status'] = $this->newsroomfuncs->get_vehicle_status();

		$data['page'] = 'NewsVehicleUpdate_view';
		$data['pkey'] = $pkey;

		// set validation rules
		$this->form_validation->set_rules('vehicle', 'pack', 'trim|required');
		$this->form_validation->set_rules('description', 'description', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('includes/layout', $data);
		} else {
			// pass validation
			$data = array(
				'vehicle' => $this->input->post('vehicle'),
				'description' => $this->input->post('description'),
				'available' => $this->input->post('available'),
				'licplate' => $this->input->post('licplate')
			);
			$this->db->where('pkey_vehicle_id', $pkey);
			$this->db->update('equip_vehicles', $data);

			//display success message
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Vehicle has been updated in the database.</div>');

			redirect('admin');
		}
	}

	public function removeVehicle($pkey)
	{
		$this->db->where('pkey_vehicle_id', $pkey);
		$this->db->update('equip_vehicles', array('archive' => '1'));
		// display success message
		$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Vehicle has been removed from the database.</div>');

		redirect('admin');
	}

	public function vehicle_check($str)
	{
		if ($str == "0") {
			$this->form_validation->set_message('vehicle_check', 'Please select a vehicle to reserve');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function date_check($st_date, $en_date)
	{
		$start = strtotime($st_date);
		$end = strtotime($en_date);

		if ($end == '') {
			$this->form_validation->set_message('date_check', 'A start and end date is required');
			return FALSE;
		} else if ($start > $end) {
			$this->form_validation->set_message('date_check', 'It appears your start date is older then your end date.');
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function name_check($str)
	{
		if ($str == "0") {
			$this->form_validation->set_message('name_check', 'Please select a MMJ for this reservation');
			return FALSE;
		} else {
			return TRUE;
		}
	}
}
