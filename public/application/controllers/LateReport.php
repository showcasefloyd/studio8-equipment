<?php
/* 
 * File Name: LateReport.php
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class LateReport extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('Equipment_model');
		$this->load->model('Vehicles_model');
	}

	//index function
	function lateEquipment()
	{

		$this->load->library('email');
		$lateE = $this->Equipment_model->get_late_equipment_report();

		foreach ($lateE as $le) {

			$this->email->from('no-reply@wtnh.com', 'Newsroom Reminder');
			$this->email->to('philip.isaacs@wtnh.com');
			$this->email->subject('Late Equipment Reminder');

			$mail = "This message is a reminder that you have checked out Equipment from News 8 and according to our records it has not been return yet.\n\r\n\r";
			$mail .= " Name: " . $le->first . " " . $le->last . "\n\r";
			$mail .= " Equipment: " . $le->pack . "\n\r";
			$mail .= " Due back on: " . $le->returndate . "\n\r\n\r";
			$mail .= " Please remember to go to " . base_url() . "index.php/equipment/checkin/" . $le->pkey . " and sign your equipment back in.\n\r\n\r Thank You. \n\r";

			$this->email->message($mail);
			$this->email->send();
		}
	}

	function lateVehicles()
	{

		$this->load->library('email');
		$lateV = $this->Vehicles_model->get_late_vehicles_report();

		foreach ($lateV as $le) {

			$this->email->from('no-reply@wtnh.com', 'Newsroom Reminder');
			$this->email->to('philip.isaacs@wtnh.com');
			$this->email->subject('Late Vehicle Reminder');

			$mail = "This message is a reminder that you have checked a Vehicle out from News 8 and according to our records it has not been returned yet.\n\r\n\r";
			$mail .= " Name: " . $le->name . "\n\r";
			$mail .= " Vehicle: " . $le->vehicle . "\n\r";
			$mail .= " Due back on: " . $le->returndate . "\n\r\n\r";
			$mail .= " Please remember to go to " . base_url() . "index.php/vehicles/checkin/" . $le->pkey_vehicle_reserve_id . " and sign your vehicle back in.\n\r\n\r Thank You. \n\r";

			$this->email->message($mail);
			$this->email->send();
		}
	}
}
