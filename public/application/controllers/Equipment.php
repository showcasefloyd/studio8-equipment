`<?php
	defined('BASEPATH') or exit('No direct script access allowed');

	class Equipment extends CI_Controller
	{

		public function __construct()
		{
			parent::__construct();
			$this->load->library('session');
			$this->load->library('NewsroomFuncs');
			$this->load->helper('form');
			$this->load->helper('url');
			$this->load->database();
			$this->load->library('form_validation');

			$this->load->model('Equipment_model');
			$this->load->model('Vehicles_model');
			$this->load->model('Mmjs_model');
		}

		public function index()
		{

			//$this->load->model('Equipment_model');
			$data['clist'] = $this->Equipment_model->get_checkedout_list();
			$data['rlist'] = $this->Equipment_model->get_reserve_list();
			$data['hlist'] = $this->Equipment_model->get_checkedout_history();

			$data['page'] = 'Equipment_view';
			$this->load->view('includes/layout', $data);
		}

		public function checkout()
		{
			$data['mmjs'] = $this->Mmjs_model->get_mmjs();

			$this->load->model('Vehicles_model');
			$data['vehicles'] = $this->Vehicles_model->get_vehicles_list();
			$data['equipment'] = $this->Equipment_model->get_equipment_list();

			$data['page'] = 'EquipmentReserve_view';

			//set validation rules
			$this->form_validation->set_rules('mmj_id', 'MMJ', 'callback_name_check');
			$this->form_validation->set_rules('reservedate', 'Date', 'trim|required|callback_date_check[' . $this->input->post('reservecidate') . ']');
			$this->form_validation->set_rules('equip_list_id', 'Equipment', 'callback_pack_check');


			if ($this->form_validation->run() == FALSE) {
				$this->load->view('includes/layout', $data);
			} else {

				//check for previous reservations
				$reserveCheck = $this->Equipment_model->checkForReserve(
					$this->input->post('reservedate'),
					$this->input->post('reservecidate'),
					$this->input->post('equip_list_id'),
					$this->input->post('vehicle_id')
				);

				if ($this->input->post('vehicle_id')) {
					//check for any reserved/checkedout Vehicles in the Vehicles table
					$checkEquimentConflicts = $this->Vehicles_model->checkForReserve(
						$this->input->post('reservedate'),
						$this->input->post('reservecidate'),
						$this->input->post('vehicle_id')
					);
				};

				if (count($reserveCheck) > 0) {
					$v = null;
					if (isset($reserveCheck[0]->vehicle)) {
						$v = "and vehicle <b>" . $reserveCheck[0]->vehicle . "</b>";
					}
					$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears there is a conflict with this reservation.  <b>" .  $reserveCheck[0]->user . "</b> has reserved <b>" . $reserveCheck[0]->pack . "</b> " . $v . " for this date and time already. It is due to be returned <b>" . $reserveCheck[0]->returndate . "</b></div>");
					$this->load->view('includes/layout', $data);
				} else if (count($checkEquimentConflicts) > 0) {

					$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears this Vehicle <b>" . $checkEquimentConflicts[0]->vehicle . "</b> is not available. It's currently reserved by <b>" .  $checkEquimentConflicts[0]->name . "</b> for this date and time already. It is due to be returned <b>" . $checkEquimentConflicts[0]->returndate . "</b></div>");
					$this->load->view('includes/layout', $data);
				} else {

					//pass validation
					$data = array(
						'mmj_id' => $this->input->post('mmj_id'),
						'equip_list_id' => $this->input->post('equip_list_id'),
						'notes' => $this->input->post('notes'),
						'date' => date("Y-m-d H:i", strtotime($this->input->post('reservedate'))),
						'returndate' => date("Y-m-d H:i", strtotime($this->input->post('reservecidate'))),
						'vehicle_id' => $this->input->post('vehicle_id')
					);

					//insert the form data into database
					$this->db->insert('equip_reserve', $data);

					//display success message
					$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Equipment request has been saved.</div>');

					redirect('equipment');
				}
			}
		}

		public function edit($pkey)
		{
			$data['mmjs'] = $this->Mmjs_model->get_mmjs();

			$this->load->model('Vehicles_model');
			$data['vehicles'] = $this->Vehicles_model->get_vehicles_list();
			$data['equipment'] = $this->Equipment_model->get_equipment_list();
			$data['record'] = $this->Equipment_model->get_equipment($pkey);

			$data['page'] = 'EquipmentUpdate_view';

			// set validation rules
			$this->form_validation->set_rules('mmj_id', 'MMJ', 'callback_name_check');
			$this->form_validation->set_rules('reservedate', 'Date', 'trim|required|callback_date_check[' . $this->input->post('reservecidate') . ']');
			$this->form_validation->set_rules('equip_list_id', 'Equipment', 'callback_pack_check');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('includes/layout', $data);
			} else {

				// check for previous reservations
				$reserveCheck = $this->Equipment_model->checkForReserve(
					$this->input->post('reservedate'),
					$this->input->post('reservecidate'),
					$this->input->post('equip_list_id'),
					$this->input->post('vehicle_id'),
					$data['record'][0]->pkey
				);


				if ($this->input->post('vehicle_id')) {
					//check for any reserved/checkedout Vehicles in the Vehicles table
					$checkEquimentConflicts = $this->Vehicles_model->checkForReserve(
						$this->input->post('reservedate'),
						$this->input->post('reservecidate'),
						$this->input->post('vehicle_id')
					);
				};

				if (count($reserveCheck) > 0) {

					$vehicle = null;
					if (isset($reserveCheck[0]->vehicle)) {
						$vehicle = "and vehicle <b>" . $reserveCheck[0]->vehicle . "</b>";
					}
					$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears there is a conflict with this reservation.  <b>" .  $reserveCheck[0]->user . "</b> has reserved <b>" . $reserveCheck[0]->pack . "</b> " . $vehicle . " for this date and time already. It is due to be returned <b>" . $reserveCheck[0]->returndate . "</b></div>");
					$this->load->view('includes/layout', $data);
				} else if (count($checkEquimentConflicts) > 0) {
					$this->session->set_flashdata('errmsg', "<div class='alert alert-danger text-center'>It appears this Vehicle <b>" . $checkEquimentConflicts[0]->vehicle . "</b> is not available. It's currently reserved by <b>" .  $checkEquimentConflicts[0]->name . "</b> for this date and time already. It is due to be returned <b>" . $checkEquimentConflicts[0]->returndate . "</b></div>");
					$this->load->view('includes/layout', $data);
				} else {

					//pass validation
					$data = array(
						'mmj_id' => $this->input->post('mmj_id'),
						'equip_list_id' => $this->input->post('equip_list_id'),
						'notes' => $this->input->post('notes'),
						'date' => date("Y-m-d H:i", strtotime($this->input->post('reservedate'))),
						'returndate' => date("Y-m-d H:i", strtotime($this->input->post('reservecidate'))),
						'vehicle_id' => $this->input->post('vehicle_id')
					);

					// insert the form data into database
					$this->db->where('pkey', $pkey);
					$this->db->update('equip_reserve', $data);
					$this->output->enable_profiler(TRUE);
					$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Equipment request has been updated.</div>');
					redirect('equipment');
				}
			}
		}

		public function checkin($pkey)
		{

			$data['mmjs'] = $this->Mmjs_model->get_mmjs();
			$this->load->model('Vehicles_model');
			$data['vehicles'] = $this->Vehicles_model->get_vehicles_list();
			$data['equipment'] = $this->Equipment_model->get_equipment_list();
			$data['record'] = $this->Equipment_model->get_equipment($pkey);
			$data['page'] = 'EquipmentCheckin_view';

			//set validation rules
			$this->form_validation->set_rules('mmj_id', 'MMJ', 'callback_name_check');
			$this->form_validation->set_rules('equipment_list_id', 'Equipment', 'callback_pack_check');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('includes/layout', $data);
			} else {
				//pass validation
				$data = array(
					'notes' => $this->input->post('notes'),
					'checkedin' => '1',
					'checkedInTime' => date("Y-m-d H:i:s")
				);
				$this->db->where('pkey', $pkey);
				$this->db->update('equip_reserve', $data);
				//display success message
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Equipment has been checked in.</div>');
				redirect('equipment');
			}
		}

		public function cancel($pkey)
		{
			$this->db->where('pkey', $pkey);
			$this->db->delete('equip_reserve');
			//display success message
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Equipment request has been cancelled.</div>');
			redirect('equipment');
		}

		public function addPack()
		{

			$data['page'] = 'PackAdd_view';
			$data['packs_status'] = $this->newsroomfuncs->get_pack_status();

			//set validation rules
			$this->form_validation->set_rules('packname', 'pack', 'trim|required');
			$this->form_validation->set_rules('packdesc', 'description', 'trim|required');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('includes/layout', $data);
			} else {
				//pass validation
				$data = array(
					'pack' => $this->input->post('packname'),
					'available' => $this->input->post('available'),
					'description' => $this->input->post('packdesc')
				);

				$this->db->insert('equip_list', $data);

				//display success message
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Equipment request has been updated.</div>');

				redirect('admin');
			}
		}

		public function updatePack($pkey)
		{

			$this->load->model('Equipment_model');
			$data['packdata'] = $this->Equipment_model->get_pack($pkey);
			$data['packs_status'] = $this->newsroomfuncs->get_pack_status();

			$data['page'] = 'PackUpdate_view';
			$data['pkey'] = $pkey;

			//set validation rules
			$this->form_validation->set_rules('packname', 'pack', 'trim|required');
			$this->form_validation->set_rules('packdesc', 'description', 'trim|required');


			if ($this->form_validation->run() == FALSE) {
				$this->load->view('includes/layout', $data);
			} else {
				//pass validation
				$data = array(
					'pack' => $this->input->post('packname'),
					'available' => $this->input->post('available'),
					'description' => $this->input->post('packdesc')
				);

				$this->db->where('pkey', $pkey);
				$this->db->update('equip_list', $data);

				//display success message
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Equipment request has been updated.</div>');

				redirect('admin');
			}
		}

		public function removePack($pkey)
		{

			$this->db->where('pkey', $pkey);
			$this->db->update('equip_list', array('archive' => '1'));

			//display success message
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Pack has been removed from the database.</div>');

			redirect('admin');
		}

		public function name_check($str)
		{
			if ($str == "0") {
				$this->form_validation->set_message('name_check', 'Please select a MMJ for this reservation');
				return FALSE;
			} else {
				return TRUE;
			}
		}

		public function pack_check($str)
		{
			if ($str == "0") {
				$this->form_validation->set_message('pack_check', 'Please select a Equipment Pack for this reservation');
				return FALSE;
			} else {
				return TRUE;
			}
		}

		public function date_check($st_date, $en_date)
		{
			$start = strtotime($st_date);
			$end = strtotime($en_date);

			if ($end == '') {
				$this->form_validation->set_message('date_check', 'A start and end date is required');
				return FALSE;
			} else if ($start > $end) {
				$this->form_validation->set_message('date_check', 'It appears your start date is older then your end date.');
				return FALSE;
			} else {
				return TRUE;
			}
		}
	}
