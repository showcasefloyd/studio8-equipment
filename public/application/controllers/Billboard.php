<?php
/* 
 * File Name: Admin.php
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Billboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('NewsroomFuncs');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->database();

		$this->load->model('Equipment_model');
		$this->load->model('Vehicles_model');
	}

	// index function
	function index()
	{

		$data['packs_status'] = $this->newsroomfuncs->get_pack_status();
		$data['vehicle_status'] = $this->newsroomfuncs->get_vehicle_status();

		// In Use
		$data['equipVlist'] = $this->Vehicles_model->get_checkedout_vehicles_list();
		$data['equipRlist'] = $this->Equipment_model->get_checkedout_list();

		// Get Available Equipment / Vehicles
		$data['availableVehicleList'] = $this->Vehicles_model->get_vehicles_available_now();
		$data['availableEquipList'] = $this->Equipment_model->get_equipment_available_now();

		// Available Equipment
		$data['equiplist'] = $this->Equipment_model->get_full_equipment_list();
		$data['vehicles'] = $this->Vehicles_model->get_full_vehicles_list();

		$this->load->view('Billboard_view_public', $data);
	}
}
