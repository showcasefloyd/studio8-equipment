<?php
/* 
 * File Name: Admin.php
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('NewsroomFuncs');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->database();
		$this->load->library('form_validation');
	}

	// index function
	public function index()
	{

		if ($this->session->userdata('logged_in')) {

			$this->load->model('Mmjs_model');
			$this->load->model('Equipment_model');
			$this->load->model('Vehicles_model');

			$data['equiplist'] = $this->Equipment_model->get_full_equipment_list();
			$data['packs_status'] = $this->newsroomfuncs->get_pack_status();
			$data['mmjs'] = $this->Mmjs_model->get_full_mmjs();
			$data['vehicles'] = $this->Vehicles_model->get_full_vehicles_list();
			$data['vehicle_status'] = $this->newsroomfuncs->get_vehicle_status();
			$data['page'] = 'Admin_view';

			$this->load->view('includes/layout', $data);
		} else {
			// If no session, redirect to login page
			redirect('verify', 'refresh');
		}
	}
}
