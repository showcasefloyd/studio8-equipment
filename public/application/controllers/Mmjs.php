<?php
/* 
 * File Name: employee.php
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mmjs extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->database();
        $this->load->library('form_validation');
    }

    //Add MMJ function
    function add()
    {
        $this->load->model('Mmjs_model');
        //fetch data from department and designation tables
        $data['department'] = $this->Mmjs_model->get_departments();

        $data['page'] = 'MmjsAdd_view';

        //set validation rules
        $this->form_validation->set_rules('firstname', 'First name', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('department', 'Department', 'callback_combo_check');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('includes/layout', $data);
        } else {
            //pass validation
            $data = array(
                'first' => $this->input->post('firstname'),
                'last' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'department_id' => $this->input->post('department'),
            );

            //insert the form data into database
            $this->db->insert('equip_mmjs', $data);

            //display success message
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">New MMJ details added to database!!!</div>');
            redirect('admin');
        }
    }

    function remove($id)
    {
        $this->db->where('pkey', $id);
        $this->db->delete('equip_mmjs');

        //display success message
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">MMJ details removed from database!</div>');
        redirect('admin');
    }

    //update function
    function update($id)
    {
        $this->load->model('Mmjs_model');

        $data['page'] = 'MmjsUpdate_view';
        $data['empno'] = $id;

        //fetch data from department and designation tables
        $data['department'] = $this->Mmjs_model->get_departments();

        //fetch employee record for the given employee no
        $data['emprecord'] = $this->Mmjs_model->get_mmj_record($id);

        //set validation rules
        $this->form_validation->set_rules('firstname', 'First name', 'trim|required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('department', 'Department', 'callback_combo_check');

        if ($this->form_validation->run() == FALSE) {
            //fail validation
            $this->load->view('includes/layout', $data);
        } else {
            //pass validation
            $data = array(
                'first' => $this->input->post('firstname'),
                'last' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'department_id' => $this->input->post('department'),
            );

            //update employee record
            $this->db->where('pkey', $id);
            $this->db->update('equip_mmjs', $data);

            //display success message
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">MMJS Record has been Successfully Updated!</div>');

            redirect('mmjs/update/' . $id);
        }
    }

    //custom validation function for dropdown input
    function combo_check($str)
    {
        if ($str == '-SELECT-') {
            $this->form_validation->set_message('combo_check', 'Valid %s Name is required');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //custom validation function to accept only alpha and space input
    function alpha_only_space($str)
    {
        if (!preg_match("/^([-a-z ])+$/i", $str)) {
            $this->form_validation->set_message('alpha_only_space', 'The %s field must contain only alphabets or spaces');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
