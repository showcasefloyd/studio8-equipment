var gulp = require('gulp');
var sass = require('gulp-sass');
var bourbon = require('node-bourbon').includePaths;
var php  = require('gulp-connect-php');
var cssnano = require('gulp-cssnano');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('php', function() {
    php.server({ base: './public', port: 8010, keepalive: true});
});

gulp.task('browser-sync',['php'], function() {
    browserSync({
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: true,
        notify: false
    });    
});

gulp.task('build',function(){
	 return gulp.src("sass/**/*.scss")
	    .pipe(sass({
	        includePaths: bourbon
	     }))
	    .pipe(cssnano())
	    //.pipe(cacheBust({type: 'timestamp' })) 
	    .pipe(gulp.dest("public/css"))
	
})

// For production move over js files 
gulp.task('copy', function() {
    gulp.src(['node_modules/jquery/dist/jquery.min.js',
              'node_modules/bootstrap/dist/js/bootstrap.min.js',
              'node_modules/jquery-ui-bundle/jquery-ui.min.js'])
    .pipe(gulp.dest('public/scripts/'));

   gulp.src(['node_modules/bootstrap/dist/css/bootstrap.min.css',
   			 'node_modules/jquery-ui-bundle/jquery-ui.min.css'])
   	.pipe(gulp.dest('public/css/'));
   	
   gulp.src(['node_modules/bootstrap/dist/fonts/*'])
     .pipe(gulp.dest('public/fonts/'));

});


// Compile sass into CSS & auto-inject into browsers
gulp.task('styles', function() {
    return gulp.src("sass/**/*.scss")
        .pipe(sass({
	        includePaths: bourbon
	     }))
        .pipe(gulp.dest("public/css"))
        .pipe(browserSync.stream());
});

//gulp.task('default', ['serve']);
gulp.task('default', ['browser-sync'], function () {;
    gulp.watch(["sass/**/*.scss"], ['styles']);
    gulp.watch(['public/**/*.php'], [reload]);  
});