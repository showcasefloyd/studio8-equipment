# Studio 8 Equipment Monitoring Tool

## Based on CodeIgniter

This project pulled in reports from multiple sources using a combination
on Cron jobs and emailed reports into a single dashboard.

---

Most of the custom code for this projects resides in the `/public/application` folder.

- /application/controllers
- /application/views
- /application/models
- /application/lib

---

### Directions

This project requires **Bower** for PHP package installation.

It also relies heavily on **Gulp** for building and running locally
